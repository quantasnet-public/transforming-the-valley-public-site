---
title: Support TRANSforming The Valley
menus:
    main:
        name: Support
---

There are multiple ways to support our efforts to transform Chippewa Valley into a place where all individuals on the gender spectrum can safely thrive.

***

## Volunteering

We’re looking for passionate individuals who are interested in helping improve the lives of trans, non-binary, and gender-conforming folk in the Chippewa Valley.

### Events

#### Ongoing volunteer areas

- Event organization & planning
- Social media
- Tech
- Recruiting volunteers & sponsors
- Professional services (hairstyling, makeup, nails, etc.)

***

## Financially

Consider sending us a monetary donation to help cover the close of services we provide free of charge to our community. Services we provide vary and may include gender-affirming clothing and accessories, hygiene and beauty products, this website, and more. In the future, we would like to increase our offerings to our community and need your help to do so!

[Donate](https://checkout.square.site/merchant/35WWYBEKZMMWZ/checkout/DAH2KNE4IYD3NA3PS747J5GS)

***

## Transgender Closet Donation Drive

![Transgender Closet Donation Drive](/images/TV_TransgenderCLosetDonDrive_2022.jpg)

Collection for our bi-monthly shopping event is ongoing! Drop off donations at any of the following locations:

Paste Google Maps Embed Code Here
