---
title: "TRANSforming The Valley"
menus:
    main:
        name: Home

theme_version: '2.8.2'
cascade:
  featured_image: '/images/ttv-facebook-banner.jpg'
---

Transforming the Valley is a volunteer organization working to provide support and resources for anyone under the transgender or gender non-conforming umbrella (including non-binary, gender fluid, intersex, etc.), in the Chippewa Valley (WI).

[Help Support Our Mission]({{< ref "/mission" >}})

# Support Groups

We host Support Groups both Virtually and in person on the First and Third Wednesday of the month.

Groups are open to Anyone who identifies as part of the transgender or gender-nonconforming community, and parents and care-givers as appropriate.

## In Person Support Group

1st Wednesday of the month

6pm - 7pm

String Theory Studio

116 North Bridge Street

Chippewa Falls, WI, 54729 [(map)](http://maps.google.com/?q=116%20North%20Bridge%20Street%20Chippewa%20Falls,%20WI,%2054729%20United%20States)

## Virtual Support Group

3rd Wednesday of the month

6pm - 7pm

Online - Check our private [facebook group](https://www.facebook.com/transformingthevalley/) or [discord](https://discord.com/invite/rGCcCxknfH) for the link!
